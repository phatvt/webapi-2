package vn.com.fsoft.mtservice.controller.query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import vn.com.fsoft.mtservice.constants.CommonConstants;
import vn.com.fsoft.mtservice.controller.BaseControllerTemplate;
import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.constant.enumeration.FunctionEnum;
import vn.com.fsoft.mtservice.object.form.RestForm;
import vn.com.fsoft.mtservice.util.NumberUtils;

/**
 * 
 * @author hungxoan
 *
 * @param <F>
 */
public abstract class QueryControllerTemplate<F extends RestForm> extends BaseControllerTemplate {

    public final ResponseEntity<String> get(IncomingRequestContext context,
                                            FunctionEnum functionEnum,
                                            RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        // check token and role
        String body = CommonConstants.EMPTY;
        body = getDefinition(context, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    public final ResponseEntity<String> get(IncomingRequestContext context,
                                            Integer id,
                                            FunctionEnum functionEnum,
                                            RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        if(NumberUtils.isEmpty(id)) {
            return reject(new ValidationStatus("500", "Request data is invalid."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = getDefinition(context, id, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    public final ResponseEntity<String> findOne(IncomingRequestContext context,
                                            String key,
                                            FunctionEnum functionEnum,
                                            RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        if(StringUtils.isBlank(key)) {
            return reject(new ValidationStatus("500", "Request data is invalid."));
        }

        // check token and role

        String body = CommonConstants.EMPTY;
        body = findOneDefinition(context, key, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    public final ResponseEntity<String> query(IncomingRequestContext context,
                                              FunctionEnum functionEnum,
                                              RequestEntity<F> requestEntity) {

        if(requestEntity == null) {
            return reject(new ValidationStatus("500", "Request is undefined."));
        }

        // check token and role
        String body = CommonConstants.EMPTY;
        body = queryDefinition(context, requestEntity);

        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }

    protected abstract String getDefinition(IncomingRequestContext context,
                                            RequestEntity<F> requestEntity);

    protected abstract String getDefinition(IncomingRequestContext context,
                                            Integer id,
                                            RequestEntity<F> requestEntity);

    protected abstract String findOneDefinition(IncomingRequestContext context,
                                                String key,
                                                RequestEntity<F> requestEntity);

    protected abstract String queryDefinition(IncomingRequestContext context,
                                              RequestEntity<F> requestEntity);

    @Override
    public ResponseEntity<String> reject(ValidationStatus validationStatus) {

        String body = rejectDefinition(validationStatus);
        return new ResponseEntity<String>(body, null, HttpStatus.OK);
    }
}
