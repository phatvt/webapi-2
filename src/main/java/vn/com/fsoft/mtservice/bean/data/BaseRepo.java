package vn.com.fsoft.mtservice.bean.data;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTemplate;

import vn.com.fsoft.mtservice.object.base.IncomingRequestContext;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ResultTransformer;
import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;
import vn.com.fsoft.mtservice.object.form.RestForm;
import vn.com.fsoft.mtservice.object.form.command.RestCommandForm;
import vn.com.fsoft.mtservice.object.helper.QueryResult;
import vn.com.fsoft.mtservice.object.helper.TransformQueryResult;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 * @param <E>
 * @param <RT>
 * @param <F>
 * @param <CF>
 */
public abstract class BaseRepo<E extends HibernateRootEntity,
        RT extends ResultTransformer,
        F extends RestForm,
        CF extends RestCommandForm> {

    private SessionFactory sessionFactory;

    public BaseRepo(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected abstract RepoStatus<E> findById(Integer id);
    protected abstract RepoStatus<List<E>> findByCode(String code);
    protected abstract RepoStatus<QueryResult<E>> query(F form);
    protected abstract RepoStatus<TransformQueryResult<RT>> transformQuery(F form);
    protected abstract RepoStatus<Object> objectQuery(F form);
    protected abstract RepoStatus<Integer> save(IncomingRequestContext context, CF commandForm);
    protected abstract RepoStatus<Integer> update(IncomingRequestContext context, CF commandForm);
    protected abstract RepoStatus<Boolean> delete(Integer id);
    protected abstract RepoStatus<Boolean> deleteBulk(List<Integer> ids);

    protected HibernateTemplate getHibernateTemplates() {
        return new HibernateTemplate(this.sessionFactory);
    }
}
