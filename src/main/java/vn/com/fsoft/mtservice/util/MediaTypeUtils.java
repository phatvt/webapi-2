////////////////////////////////////////////////////////////
// MediaTypeUtils.java
// Version: 1.0
// Author: (FPT) PhatVT1
// Create date: 2018/05/26
// Update date: -
////////////////////////////////////////////////////////////
package vn.com.fsoft.mtservice.util;

import javax.servlet.ServletContext;

import org.springframework.http.MediaType;
 
public class MediaTypeUtils {

    public static MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
        String mineType = servletContext.getMimeType(fileName);
        try {
            MediaType mediaType = MediaType.parseMediaType(mineType);
            return mediaType;
        } catch (Exception e) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
     
}
