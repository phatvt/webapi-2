package vn.com.fsoft.mtservice.object.base;



import java.util.List;

import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class LoginStatus<T extends HibernateRootEntity> extends Status {

    private Boolean first;
    private String fullName;
    private List<T> data;

    public LoginStatus() {
        super();
    }

    public LoginStatus(String code, String message) {
        super(code, message);
    }

    public LoginStatus(String code, String message, Boolean first, String fullName) {
        super(code, message);
        this.first = first;
        this.fullName = fullName;
    }

    public LoginStatus(String code, String message, Boolean first, String fullName, List<T> data) {
        super(code, message);
        this.first = first;
        this.fullName = fullName;
        this.data = data;
    }

    public Boolean getFirst() {
        return first;
    }

    public void setFirst(Boolean first) {
        this.first = first;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
