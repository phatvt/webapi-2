package vn.com.fsoft.mtservice.object.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.fsoft.mtservice.object.form.query.LoginHistoryQueryForm;

/**
 * 
 * @author hungxoan
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginHistoryForm extends RestForm {
	private LoginHistoryQueryForm query;

	public LoginHistoryQueryForm getQuery() {
		return query;
	}

	public void setQuery(LoginHistoryQueryForm query) {
		this.query = query;
	}

}
