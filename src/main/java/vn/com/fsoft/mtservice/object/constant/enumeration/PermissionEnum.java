package vn.com.fsoft.mtservice.object.constant.enumeration;

/**
 * hungxoan
 */

public enum PermissionEnum {
    VIEW("View"),
    EXPORT("Export"),
    INSERT("Insert"),
    UPDATE("Update"),
    IMPORT("Import"),
    APPROVAL("Approval"),
    DELETE("Delete");

    private String permissionType;

    PermissionEnum(String permissionType) {
        this.permissionType = permissionType;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }
}
