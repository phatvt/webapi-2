package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

import javax.persistence.*;
import java.io.Serializable;


@Entity(name = "AuthorityEntity")
@Table(name = "authority", schema = DatabaseConstants.SCHEMA)
public class AuthorityEntity extends TrackingFieldEntity implements Serializable {
	
	@Id
    @Column(name = "id")
	@GeneratedValue(generator = "authority_value_generator",
     		strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "authority_value_generator", schema = DatabaseConstants.SCHEMA,
			sequenceName = "authority_seq")
    private Integer id;
	
	@JoinColumn(name = "role_id")
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	private RoleEntity role;
	
	@JoinColumn(name = "function_id")
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	private MasterDataTableEntity function;
	
	@JoinColumn(name = "role_permission_id")
	@ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
	private MasterDataTableEntity permission;
	
	public AuthorityEntity() {
		super();
	}

	@JsonView(Views.Public.class)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonView(Views.RelationBackward.class)
	public RoleEntity getRole() {
		return role;
	}

	public void setRole(RoleEntity role) {
		this.role = role;
	}

	@JsonView(Views.Public.class)
	public MasterDataTableEntity getFunction() {
		return function;
	}

	public void setFunction(MasterDataTableEntity function) {
		this.function = function;
	}

	@JsonView(Views.Public.class)
	public MasterDataTableEntity getPermission() {
		return permission;
	}

	public void setPermission(MasterDataTableEntity permission) {
		this.permission = permission;
	}
}
