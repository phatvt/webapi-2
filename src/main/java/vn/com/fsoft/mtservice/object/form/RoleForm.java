package vn.com.fsoft.mtservice.object.form;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.fsoft.mtservice.object.form.command.RoleCommandForm;
import vn.com.fsoft.mtservice.object.form.query.RoleQueryForm;

/**
 * 
 * @author hungxoan
 *
 */

@JsonIgnoreProperties( ignoreUnknown = true)
public class RoleForm extends RestForm {

    private RoleCommandForm model;
    private RoleQueryForm query;

    public RoleForm() {}

    public RoleCommandForm getModel() {
        return model;
    }

    public void setModel(RoleCommandForm model) {
        this.model = model;
    }

    public RoleQueryForm getQuery() {
        return query;
    }

    public void setQuery(RoleQueryForm query) {
        this.query = query;
    }
}
