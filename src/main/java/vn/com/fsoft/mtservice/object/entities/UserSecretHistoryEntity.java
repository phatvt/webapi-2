package vn.com.fsoft.mtservice.object.entities;



import javax.persistence.*;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;

@Entity(name = "UserSecretHistoryEntity")
@Table(name = "user_secret_history", schema = DatabaseConstants.SCHEMA)
public class UserSecretHistoryEntity extends TrackingFieldEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "secret_history_value_generator", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "secret_history_value_generator", schema = DatabaseConstants.SCHEMA, sequenceName = "secret_history_seq")
    private Integer id;

    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "secret_1")
    private String secret1;

    @Column(name = "secret_2")
    private String secret2;

    @Column(name = "secret_3")
    private String secret3;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getSecret1() {
        return secret1;
    }

    public void setSecret1(String secret1) {
        this.secret1 = secret1;
    }

    public String getSecret2() {
        return secret2;
    }

    public void setSecret2(String secret2) {
        this.secret2 = secret2;
    }

    public String getSecret3() {
        return secret3;
    }

    public void setSecret3(String secret3) {
        this.secret3 = secret3;
    }


}