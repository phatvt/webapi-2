package vn.com.fsoft.mtservice.object.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

/**
 * 
 * @author hungxoan
 *
 */
public class HibernateAwareObjectMapper extends ObjectMapper {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HibernateAwareObjectMapper() {
        registerModule(new Hibernate5Module());
    }
}
