package vn.com.fsoft.mtservice.object.entities;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.constants.DatabaseConstants;
import vn.com.fsoft.mtservice.util.Views;

import javax.persistence.*;

/**
 * 
 * @author hungxoan
 *
 */

@Entity(name = "FunctionPermissionEntity")
@Table(name = "function_permissions", schema = DatabaseConstants.SCHEMA)
public class FunctionPermissionEntity extends HibernateRootEntity {

    @Id
    @Column(name = "id")
    private Integer id;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "function_id")
    private MasterDataTableEntity function;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "permission_id")
    private MasterDataTableEntity permission;

    public FunctionPermissionEntity() {
    }

    @JsonView(Views.Public.class)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonView(Views.Public.class)
    public MasterDataTableEntity getFunction() {
        return function;
    }

    public void setFunction(MasterDataTableEntity function) {
        this.function = function;
    }

    @JsonView(Views.Public.class)
    public MasterDataTableEntity getPermission() {
        return permission;
    }

    public void setPermission(MasterDataTableEntity permission) {
        this.permission = permission;
    }
}
